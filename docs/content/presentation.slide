<!-- GenericGenerator: true
--   output: html
--   cmdline: pandoc -f markdown -t slidy --css=css/presentation.css --section-divs -s -o {outname} {filename}
-->


![](me-collage.png)

Career Summary:\
<big>Milton Soares Filho</big>\
37 years old, married with child\
15 years experience with Linux and embedded

## Disclaimer

Presentation done in pure-text using markdown and the following tools:

- Pandoc: markdown to slides transformation
- Pelican: static site generator
- ImageMagik: graphs and collages
- CSS: custom styles
- Git: revision control


Available online at https://goo.gl/UOb5jU


## 2002~2004 Power Plant Monitoring and Control @ Lactec/Copel

![](lactec-project.png)

- QNX: Unix-like micro-kernel with messaging framework
- Distributed Architecture
- Port to Linux side-project


## 2004~2006 Siemens' XMPM @ Conectiva/Mandriva

Siemens' XMPM: New Mobile Phone Manager for Linux

![](mandriva-project.png)


## XMPM Reference

![](images/xmpm-page.png)

## Biggest Challenges

- Autotools
- Portability on 3 different flavors of Linux
- Core Utilities vs UI communication using gSoap as IPC
- Setting Customer expectations


![](mandriva-collage.png)

## Moto Trip on Brazilian Coast

![](../content/images/moto-trip-coast-collage.png)

## 2007~2008, 2011~2012 AOC/Envision's ISDB-T set-top-box @ Fucapi

![](fucapi-project.png)

## Biggest Challenges

- Maintain the image creation process
- Keep up with evolving Brazilian Digital TV Standards
- Connect Low-level drivers API with High-level UI and middleware


![](images/buildroot.png)

## 2009 Nokia's Qt Enablement @ INdT


Challenge: improve Qt for very restricted mobile eco-systems

- Symbian and Maemo OS
- Participated on Qt development (4.x series)
- Qt enablement on Nokia devices
- OpenBossa conference


![](nokia-collage.png)

## 2013~Now Telefonica Vivo's set-top-box @ Telefonica

Set-top-box digital TV for broadband company triple-play.

![](telefonica-collage.png)


## Project Overview

![](telefonica-project.png)

## Biggest Challenges


- Sew it togheter and provide a great product for the Client
- Create a Continuous Integration environment for the Team
- Push Qt/QML technologies to the Company


![](telefonica-tools.png)

<!--

## Technical High-School @1994

CEFET-PR

- Electronics
- Circuits
- Math
- Assembly Programming + Digital Electronics


## Undergraduate at UFPR @1998

Challenge: learn programming and Linux

- Computer Science
- Algorithms
- Graphs
- Only Linux labs (dumb terminals project)
- Devoured tons of manpages (awk, sed, find, grep, all unix power tools, bash,
  etc)
- Installed Conectiva Guarani distribution at home


## Lacte - Copel @2002

Challenge: integrate third-party libraries on existent monitoring system

- Sub-station monitoring and management system
- QNX micro-kernel unix-like
- Distributed Network Protocol
- IEC 60870-5 Control and Monitoring Protocol


## Conectiva/Mandriva @2004

Challenge: coach contractor about GNU/Linux best practices and deliver product
value incrementaly and satisfactory

- Moved to Manaus/AM
- Siemens' contracted
- Mobile phone management on Linux Desktop
- Started learning Agile Methodologies
- web-services, soap, JNI integration
- Further sold to BenQ and closed


## Hiatus: Brazilian Coast Ride

- Photos?


## Fucapi @2007

Challenge: understand and develop freshly defined Brazilian Digital TV Standard

- Technology Institute
- Brazilian Digital TV initial efforts
- Buildroot first contact
- Envision / AOC


## INdT - Nokia's Institute @2009

Challenge: improve Qt for very restricted mobile eco-systems

- Symbian and Maemo OS
- Participated on Qt development (4.x series)
- Qt enablement on Nokia devices
- OpenBossa conference


## Hiatus: Brazilian Mid-West Ride

## M2Smart @2010

Challenge: develop Brazilian regulations in a sane way

- Small commerce suite
- Fiscal Printer driver
- PostgresSQL contact


## Fucapi @2011

Challenge: integrate solution with other parties

- More focused on Brazilian's middleware (Ginga-NCL)
- Lua Bindings
- More Buildroot


## GVT @2013

Challenge: get a mixed cloud-solution to work in a scenario mostly offline

- Back to birth city
- Young networking company enabling triple-play (phone, internet and TV)
- Initially a mixed local and cloud solution
- Moved to a mostly embedded solution to mitigate conectivity issues


## Telefonica @2015

Challenge: prove skills to Spain headquarters and introduce Qt/QML/C++
development within the teammates

- Maintenance of legacy products
- Huge project with Qt + QML for future products
- Scrum-based
- Arris, Pace, NDS, Cisco, Sagemcom, Kaon, Nagra, Verimatrix, PlayReady, Youtube
- Enabled Gitlab Continuous Integration and advocated better practices on code
  development

-->

## Future

- Looking forward for even greater challenges
- Eager to know clever people and learn from them
- Even more focused on team building


<!-- vim: set filetype=markdown :-->
