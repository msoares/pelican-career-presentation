# GenericGenerator: true
# output: png
# cmdline: sh {filename} {outname}

outfile=$1

images=content/images/roadway.jpg
images="${images} content/images/wife-child.jpg"
images="${images} content/images/gnu-linux.png"

#montage ${images} \
#	-border 2x2 -background white +polaroid -tile x4 ${outfile}

montage ${images} \
    -border 1x1 +polaroid -background 'rgb(86,120,169)' -tile 1x \
    -thumbnail 300x300 -geometry 400x200+5+5 ${outfile}
