# GenericGenerator: true
# output: png
# cmdline: sh {filename} {outname}

outfile=$1

images=""
images="${images} content/images/gitlab-logo.jpg"
images="${images} content/images/gitlab-ci.png"
images="${images} content/images/qt-logo.png"

montage ${images} -background white -tile x1 ${outfile}
