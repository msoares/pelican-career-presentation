# GenericGenerator: true
# output: png
# cmdline: sh {filename} {outname}

outfile=$1

images=content/images/mandriva-logo.jpg
images="${images} content/images/siemens-logo.png"
images="${images} content/images/suse-logo.png"
images="${images} content/images/fedora-logo.png"
images="${images} content/images/scrum-process.jpg"

#montage ${images} \
#	-border 2x2 -background white +polaroid -tile x4 ${outfile}

montage ${images} -border 1x1 -background transparent -tile x1 ${outfile}
