# GenericGenerator: true
# output: png
# cmdline: sh {filename} {outname}

outfile=$1

images=""
images="${images} content/images/gvt/broadcom-logo.jpg"
images="${images} content/images/gvt/stmicro-logo.jpg"
images="${images} content/images/gvt/cisco-logo.png"
images="${images} content/images/gvt/verimatrix-logo.png"
images="${images} content/images/gvt/nagra-logo.png"
images="${images} content/images/gvt/kaon-logo.jpg"
images="${images} content/images/gvt/sagemcom-logo.png"
images="${images} content/images/gvt/arris-logo.jpg"
images="${images} content/images/gvt/pace-logo.jpg"
images="${images} content/images/gvt/wyplay-logo.jpg"
images="${images} content/images/gvt/ericsson-logo.jpg"
images="${images} content/images/gvt/dolby-logo.jpg"
#images="${images} content/images/gvt/mirada-logo.png"
#images="${images} content/images/gvt/gvt-logo.jpg"
images="${images} content/images/gvt/youtube-logo.png"
images="${images} content/images/gvt/tlf-logo.jpg"
images="${images} content/images/gvt/vivo-logo.jpg"

montage ${images} -background white -tile 5x3 ${outfile}
