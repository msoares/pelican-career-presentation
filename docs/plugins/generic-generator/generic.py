import glob
import os
import os.path
import re
import subprocess
import logging

from pelican import signals
from pelican.generators import Generator

logger = logging.getLogger(__name__)

class GenericGenerator(Generator):
    "Run custom scripts to generate output files"

    def __init__(self, *args, **kwargs):
        Generator.__init__(self, *args, **kwargs)
        super(GenericGenerator, self).__init__(*args, **kwargs)

    def generate_context(self):
        # filter all files which are gonna use GenericGenerator services
        self.generic_files = list()

        for filename in glob.glob(os.path.join(self.settings['PATH'], '*')):
            if not os.path.isfile(filename): continue
            meta = self.get_metadata(filename)
            if meta.has_key('GenericGenerator') and meta.has_key('cmdline'):
                logger.info('GenericGenerator flag found on file {}'.format(filename))
                self.generic_files.append(filename)
                # TODO: remove from all articles/posts lists


    def get_metadata(self, filename):
        with open(filename) as fp:
            text = fp.readlines()

        metadata = {}
        # default output extension
        metadata['filename'] = filename
        metadata['output'] = 'out'
        metadata['basename'] = os.path.basename(filename)

        pat = re.compile('^(\S+)\s+(\w+):\s+(.+)')

        for i, line in enumerate(text):
            match = pat.match(line)
            if match:
                name, value = match.group(2).strip(), match.group(3)
                metadata[name] = value
            else:
                break

        # alias for output filename
        metadata['outname'] = os.path.join(self.output_path,
                '.'.join((os.path.splitext(metadata['basename'])[0], metadata['output'])))

        return metadata


    def generate_output(self, writer=None):
        for name in self.generic_files:
            meta = self.get_metadata(name)
            try:
                if not os.path.exists(meta['outname']) or \
                        os.path.getmtime(meta['filename']) > os.path.getmtime(meta['outname']):
                    logger.debug('Processing file {filename} -> {outname}'.format(**meta))
                    cmdline = meta['cmdline'].format(**meta).split()
                    logger.debug('call: {}'.format(cmdline))
                    subprocess.call(cmdline)
                else:
                    logger.debug('Skipping file {filename}'.format(**meta))
            except KeyError:
                pass


def get_generators(generators):
    return GenericGenerator

def register():
    signals.get_generators.connect(get_generators)
